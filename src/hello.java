import javax.swing.tree.TreeNode;
import java.util.Enumeration;

public class hello {
    public static void main(String []args)
    {
        System.out.println("helloworld");
    }

        public TreeNode buildTree(int[] inorder, int[] postorder) {
            return buildTreeHelper(inorder,0,inorder.length-1,postorder,0,postorder.length-1);

        }
        public TreeNode buildTreeHelper(int []inorder,int i_begin,int i_end,int []postorder,int p_begin,int p_end)
        {
            if(p_begin==p_end)
                return null;

            int root_val=postorder[p_end];
            TreeNode root= new TreeNode(root_val) ;


            int root_local=0;
            int i=0;
            while(i<i_end)
            {
                if(inorder[i]==root_val)
                {
                    root_local=i;
                    break;
                }
                i++;

            }
            int len=root_local-i_begin;
            root.left=buildTreeHelper(inorder,i_begin,root_local-1,postorder,p_begin,len+p_begin);
            root.right=buildTreeHelper(inorder,root_local+1,i_end,postorder,p_begin+len+1,p_end-1);
            return root;
        }
    }

